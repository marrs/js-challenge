angular.module('chat', ['components', 'emoticon', 'ngSanitize']).
  config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/main', {templateUrl: 'partials/chat.html',   controller: ChatCtrl}).
      otherwise({redirectTo: '/main'});
}]);

angular.module('components', [])
	.directive('login', function() {
		return {
			restrict: 'E',
			controller: 'LoginCtrl',
			
			templateUrl: 'partials/login.html'
		}
	}
);
