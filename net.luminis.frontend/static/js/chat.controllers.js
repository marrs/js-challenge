var ChatCtrl = ['$scope', '$http', function($scope, $http) {
	$scope.timeWatched =[];

	$scope.$watch('username', function() {
		if($scope.username != undefined && $scope.username.length > 0) {
			$scope.poll();	
		}
	});
	
	
	$scope.requestNotifications = function(msg){
		
		
		  if (window.webkitNotifications.checkPermission() == 0) { // 0 is PERMISSION_ALLOWED
			  if(msg){
				  if(msg.sender == $scope.username){
					  return false;
				  }
				  
				  notification_msg = window.webkitNotifications.createNotification(
						  'icon.png', msg.sender+" says:", "'"+msg.content+"'");
				  notification_msg.show();
			  } else {
				  notification_msg = window.webkitNotifications.createNotification(
						  'icon.png', 'desktop notifications', "desktop notifications zijn ingeschakeld");
				  notification_msg.show();
			  }
		  } else {
		    window.webkitNotifications.requestPermission();
		  }
	}
	
	
	$scope.poll = function() {
		$http.get('/server/' + $scope.username).success(function(data) {
			if($scope.messages == undefined) {
				$scope.messages = data;
			} else {
				angular.forEach(data, function(msg) {
					var found = false;
					angular.forEach($scope.messages, function(existingMsg) {
						if(msg.id == existingMsg.id) {
							found = true;
							return false;
						} 
					});
					if(!found) {
						$scope.requestNotifications(msg);				
						$scope.messages.push(msg);
						$scope.messages.sort(compareMessages);
					}
				});
			}
			$http.get('/server/' + $scope.sendTo).success(function(data) {
				if($scope.messages == undefined) {
					$scope.messages = data;							
				} else {
					angular.forEach(data, function(msg) {
						if (msg.sender == $scope.username) {
							var found = false;
							angular.forEach($scope.messages, function(existingMsg) {
								if(msg.id == existingMsg.id) {
									found = true;
									return false;
								} 
							});
							if (!found) {
								$scope.messages.push(msg);
								$scope.messages.sort(compareMessages);
							}
						}
					});
				}
			});	
		}); 

		setTimeout($scope.poll, 1000);
		$scope.getNewMsgCount();
	};
	
	
	$scope.getNewMsgCount = function(){		
		angular.forEach($scope.timeWatched, function(tsUser) {
			var count = 0;
			if (tsUser.username != $scope.sendTo) {
				angular.forEach($scope.messages, function(msg) {
					if(msg.sender == tsUser.username){
						var timestamp = Date.parse(msg.timestamp);
						if(timestamp > tsUser.timestamp || tsUser.timestamp == null){
							count++
						}
					}	
				});
			}
			tsUser.count = count;
		});
	}
	
	$scope.setMsgReceiver = function(user) {
		
		if(user.name) {
			user = user.name
		}
		
		var time = new Date().getTime();

		if ($scope.sendTo != user) {
			angular.forEach($scope.timeWatched, function(tsUser) {
				if (tsUser.username == $scope.sendTo) {
					tsUser.timestamp = time;
				}
			});
			$scope.sendTo = user;
			$scope.messages = undefined;
		}
		
		$('.chatInput').focus();
	}
	
	$scope.send = function(message) {

		$http.post('/server/' + $scope.sendTo, {
			sender: $scope.username,
			content: message
		}).success(function(data) {
			$scope.message = "";
		});	    	
	}

	$scope.fetchUsers = function() {		
		
		$http.get('/server').success(function(data) {	
			if($scope.users == undefined) {
				$scope.users = data;							
						
			} else {
				var updated = false;
				angular.forEach(data, function(user) {
					var found = false;

					angular.forEach($scope.users, function(existingUser) {
						if(user.name == existingUser.name) {
							found = true;
							return false;
						}
					});
					if(!found) {
						$scope.userAdded(user);
						updated = true;						
					}
					$scope.users = data
				}); 	

				
				
					angular.forEach($scope.users, function(user) {
						var set = true;
						angular.forEach($scope.timeWatched, function(userx){
							if(user.name == userx.username){
								set = false;
							}
						});
						if(set){
							var add = {'username': user.name, 'timestamp': null, 'count': 0}
							$scope.timeWatched.push(add);
						}
					});
					$('.chatInput').focus();
				
			}
			if ($scope.users.length > 1 && $scope.sendTo == undefined) {
				angular.forEach($scope.users, function(userx) {
					if (userx.name != $scope.username) {
						$scope.sendTo = userx.name;
						return;
					}
				});
			}
		});	    

		setTimeout($scope.fetchUsers, 3000);
	};

	$scope.userAdded = function(user) {
		console.log("User added: " + user.name)
	}


	$scope.fetchUsers();
}];

var LoginCtrl = ['$scope', '$http', function($scope, $http) {
	$scope.userExists = false;
	$scope.login = function(username) {
		var found = false;
		angular.forEach($scope.users, function(userx) {
			if (userx.name == username) {
				found = true;
				$scope.username = username;
				$scope.userExists = false;
			}
		});
		if (!found) {
			$http.post('/server', {name: username})
			.success(function(data) {
				console.log('successfully logged in');
				$scope.username = username;
				$scope.userExists = false;
			}).
			error(function(data){
				$scope.username = username;
				$scope.userExists = false;
			});
		}
	}
}];

// Sorts messages by date
function compareMessages(a, b) {
	var ta = Date.parse(a.timestamp);
	var tb = Date.parse(b.timestamp);
	if (ta < tb) {
		return -1;
	}
	else if (ta > tb) {
	    return 1;
	}
	else {
		return 0;
	}
}
