angular.module('emoticon', []).filter('emoticon', function() {
    return function(input) {
        // see http://messenger.msn.com/Resource/Emoticons.aspx for the whole list, I did not copy everything
        // from http://stackoverflow.com/questions/3055515/replace-a-list-of-emoticons-with-their-images
        var emoticons = {
            ':-)' : 'regular_smile.gif',
            ':)'  : 'regular_smile.gif',
            ':D'  : 'teeth_smile.gif',
            ':-D' : 'teeth_smile.gif',
            ':-(' : 'sad_smile.gif',
            ':(' : 'sad_smile.gif',
            ':-O' : 'omg_smile.gif',
            ':O' : 'omg_smile.gif',
            ':o' : 'omg_smile.gif',
            ':-P' : 'tongue_smile.gif',
            ':P' : 'tongue_smile.gif',
            ':p' : 'tongue_smile.gif',
            ';)' : 'wink_smile.gif',
            ';-)' : 'wink_smile.gif'
        }, url = "http://messenger.msn.com/MMM2006-04-19_17.00/Resource/emoticons/", patterns = [],
        metachars = /[[\]{}()*+?.\\|^$\-,&#\s]/g;
        // build a regex pattern for each defined property
        for (var i in emoticons) {
            if (emoticons.hasOwnProperty(i)) { // escape metacharacters
                patterns.push('('+i.replace(metachars, "\\$&")+')');
            }
        }
        // build the regular expression and replace
        return input.replace(new RegExp(patterns.join('|'),'g'), function (match) {
            return typeof emoticons[match] != 'undefined' ?
            '<img src="' + url + emoticons[match] + '"/>' :
            match;
        });
    };
}).filter('color', function() {
    return function(input){
        // Calculate unique color for this user
        var node_fill_colors = ["rgba(244,185,0,1)","rgba(25,82,186,1)","rgba(179,67,175,1)","rgba(25,118,34,1)","rgba(148,61,61,1)","rgba(98,64,23,1)","rgba(146,186,25,1)","rgba(177,21,21,1)","rgba(53,168,216,1)","rgba(25,118,117,1)","rgba(128,44,199,1)","rgba(64,48,186,1)","rgba(73,156,32,1)","rgba(55,123,226,1)","rgba(201,207,42,1)","rgba(209,104,10,1)","rgba(100,109,36,1)","rgba(72,88,107,1)","rgba(140,147,34,1)","rgba(106,87,129,1)","rgba(124,7,7,1)","rgba(90,42,50,1)","rgba(178,28,91,1)","rgba(37,88,165,1)"];
        var a = 0;
        for (var i = 0; i < input.length; i++) {
            a += input.charCodeAt(i);
        }
        a = Number(String(a).split("").reverse().join(""));
        var color_index = Math.floor((a / Math.pow(10,String(a).length)) * (node_fill_colors.length));
        var color = node_fill_colors[color_index];
        return color;
    }
});
