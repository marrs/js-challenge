basePath = '../../';

files = [
  JASMINE,
  JASMINE_ADAPTER,
  'static/js/lib/angular.js',
  'static/js/lib/angular-*.js',
  'jstest/lib/angular-mocks.js',
  'static/js/**/*.js',
  'jstest/specs/**/*.js'
];

autoWatch = true;

browsers = ['Chrome'];

junitReporter = {
  outputFile: 'test_out/unit.xml',
  suite: 'unit'
};
