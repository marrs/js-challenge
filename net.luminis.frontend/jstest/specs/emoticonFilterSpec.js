'use strict';

describe('filter', function() {
  beforeEach(module('emoticon'));
  describe('emoticon', function() {
    it('should convert a smiley to an emoticon image',
        inject(function(emoticonFilter) {
      expect(emoticonFilter("smile")).toBe('smile');
      expect(emoticonFilter(":)")).toBe('<img src="http://messenger.msn.com/MMM2006-04-19_17.00/Resource/emoticons/regular_smile.gif"/>');
      expect(emoticonFilter("cool :D stuff")).toBe('cool <img src="http://messenger.msn.com/MMM2006-04-19_17.00/Resource/emoticons/teeth_smile.gif"/> stuff');
      expect(emoticonFilter(":D :D")).toBe('<img src="http://messenger.msn.com/MMM2006-04-19_17.00/Resource/emoticons/teeth_smile.gif"/> <img src="http://messenger.msn.com/MMM2006-04-19_17.00/Resource/emoticons/teeth_smile.gif"/>');
    }));
  });
});
