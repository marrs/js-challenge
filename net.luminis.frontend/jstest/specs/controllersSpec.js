'use strict';

describe('ChatCtrl', function(){
  var scope, ctrl, $httpBackend, $location, $browser;
  var users = [{name: 'A'},{name: 'B'}];
  var messages = [
  	{sender: 'firstuser', content: 'message 1'},
  	{sender: 'seconduser', content: 'message 2'},
  	{sender: 'seconduser', content: 'message 3'},
  ];
  
  beforeEach(inject(function(_$httpBackend_, $rootScope, $controller, _$location_, _$browser_) {
	    $httpBackend = _$httpBackend_;
	    $location = _$location_;
	    $browser = _$browser_;

	    scope = $rootScope.$new();

	    $httpBackend.when('GET', '/server').respond(users);
	    ctrl = $controller(ChatCtrl, {$scope: scope});

		$httpBackend.flush();
	  }));

  it('controller should be instantiable', function() {
	expect(ctrl).toBeDefined();
  });


  it('should put users in $scope', function() {
  	expect(scope.users).toBeDefined();
    expect(scope.users).toEqual(users);
  });

  it('should put messages in scope', function() {
  	scope.username = 'someuser';
	$httpBackend.when('GET', '/server/someuser').respond(messages);
  $httpBackend.when('GET', '/server/B').respond(messages);

  	scope.poll();
  	$httpBackend.flush();
  	expect(scope.messages).toEqual(messages);
  });

  it('should load messages when the username is changed', function() {
	$httpBackend.when('GET', '/server/someuser').respond(messages);
  $httpBackend.when('GET', '/server/B').respond(messages);
	scope.username = 'someuser';
	scope.$digest();
	
  	$httpBackend.flush();
  	expect(scope.messages).toEqual(messages);
  });

  it('should send messages', function() {
  	$httpBackend.expectPOST('/server/someuser', {sender: 'me', content: 'some message'}).respond(200);
  	scope.sendTo = 'someuser';
  	scope.username = 'me';

  	scope.send("some message");
  	$httpBackend.verifyNoOutstandingExpectation();
  });

  it('should reset the message variable after sending a message', function() {
  	scope.message = "somemessage";

  	$httpBackend.expectPOST('/server/someuser', {sender: 'me', content: 'some message'}).respond(200);
    
  	$httpBackend.when('GET', '/server/me').respond(messages);
    $httpBackend.when('GET', '/server/someuser').respond(messages);
  	scope.sendTo = 'someuser';
  	scope.username = 'me';

  	scope.send("some message");
  	$httpBackend.flush();

  	$httpBackend.verifyNoOutstandingExpectation();

  	expect(scope.message).toEqual("");
  });
});

describe('LoginCtrl', function(){
  var scope, ctrl, $httpBackend, $location, $browser;
  
  beforeEach(inject(function(_$httpBackend_, $rootScope, $controller, _$location_, _$browser_) {
    $httpBackend = _$httpBackend_;
    $location = _$location_;
    $browser = _$browser_;

    scope = $rootScope.$new();

    ctrl = $controller(LoginCtrl, {$scope: scope});	
  }));

  it('should POST to register a user', function() {
  	$httpBackend.expectPOST('/server', {name: 'me'}).respond(200);
  	scope.login("me");
  	$httpBackend.verifyNoOutstandingExpectation();

  });
 });
