'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('chat app', function() {

  beforeEach(function() {
    browser().navigateTo('/index.html');
  });


  it('should automatically redirect to /main when location hash/fragment is empty', function() {
    expect(browser().location().url()).toBe("/main");
  });
});
