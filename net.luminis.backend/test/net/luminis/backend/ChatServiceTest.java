package net.luminis.backend;

import java.util.List;

import junit.framework.TestCase;

public class ChatServiceTest extends TestCase {
    private static final String JOHN_DOE = "john.doe";
    private static final String JANE_DOE = "jane.doe";

    private ChatService m_service;
    private User m_userJohnDoe;
    private User m_userJaneDoe;

    public void testAddNonExistingUserOk() {
        m_service.addUser(m_userJaneDoe);
        m_service.addUser(m_userJohnDoe);

        List<User> users = m_service.getUsers();
        assertTrue(users.contains(m_userJohnDoe));
        assertTrue(users.contains(m_userJaneDoe));
    }

    public void testAddNullUserFail() {
        try {
            m_service.addUser(null);
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testAddExistingUserFail() {
        m_service.addUser(m_userJohnDoe);

        try {
            m_service.addUser(m_userJohnDoe);
            fail("Expected IllegalStateException!");
        }
        catch (IllegalStateException exception) {
            // Ok; expected...
        }
    }

    public void testRemoveNullUserFail() {
        try {
            m_service.removeUser(null);
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testRemoveNonExistingUserFail() {
        try {
            m_service.removeUser(m_userJaneDoe);
            fail("Expected IllegalStateException!");
        }
        catch (IllegalStateException exception) {
            // Ok; expected...
        }
    }

    public void testRemoveExistingUserOk() {
        m_service.addUser(m_userJaneDoe);
        m_service.addUser(m_userJohnDoe);

        m_service.removeUser(m_userJaneDoe);

        List<User> users = m_service.getUsers();
        assertTrue(users.contains(m_userJohnDoe));
        assertFalse(users.contains(m_userJaneDoe));
    }

    public void testGetUsersInitiallyReturnsEmptyList() {
        List<User> users = m_service.getUsers();
        assertNotNull(users);
        assertTrue(users.isEmpty());
    }

    public void testGetUsersReturnsSnapshotOfUsers() {
        List<User> users = m_service.getUsers();
        assertNotNull(users);
        assertTrue(users.isEmpty());

        m_service.addUser(m_userJaneDoe);

        assertTrue(users.isEmpty());
    }

    public void testSendMessageWithNullRecipientFail() {
        m_service.addUser(m_userJohnDoe);

        try {
            m_service.sendMessage(null, new Message(JOHN_DOE, "message"));
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testSendMessageWithNonExistingRecipientFail() {
        m_service.addUser(m_userJohnDoe);

        try {
            m_service.sendMessage(m_userJaneDoe, new Message(JOHN_DOE, "message"));
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testSendMessageWithNonExistingSenderFail() {
        m_service.addUser(m_userJaneDoe);

        try {
            m_service.sendMessage(m_userJaneDoe, new Message(JOHN_DOE, "message"));
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testSendMessageWithoutSenderFail() {
        m_service.addUser(m_userJaneDoe);

        try {
            m_service.sendMessage(m_userJaneDoe, new Message(null, "message"));
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testSendMessageWithoutMessageFail() {
        m_service.addUser(m_userJaneDoe);

        try {
            m_service.sendMessage(m_userJaneDoe, new Message(JANE_DOE, null));
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testSendMessageOk() {
        m_service.addUser(m_userJohnDoe);
        m_service.addUser(m_userJaneDoe);

        Message msg = new Message(JOHN_DOE, "message");
        m_service.sendMessage(m_userJaneDoe, msg);
    }

    public void testSendMessagesWithExistingIdFail() {
        m_service.addUser(m_userJohnDoe);
        m_service.addUser(m_userJaneDoe);

        Message msg1 = new Message(JOHN_DOE, "message1");
        m_service.sendMessage(m_userJaneDoe, msg1);

        try {
            m_service.sendMessage(m_userJaneDoe, msg1);
            fail("Expected IllegalStateException!");
        }
        catch (IllegalStateException exception) {
            // Ok; expected...
        }
    }

    public void testGetMessagesForNullUserFail() {
        try {
            m_service.getMessagesFor(null);
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testGetMessagesForNonExistingUserFail() {
        try {
            m_service.getMessagesFor(m_userJaneDoe);
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testGetMessagesForExistingUserInitiallyReturnsEmptyListOk() {
        m_service.addUser(m_userJaneDoe);

        List<Message> messages = m_service.getMessagesFor(m_userJaneDoe);
        assertNotNull(messages);
        assertTrue(messages.isEmpty());
    }

    public void testGetMessagesForExistingUserOk() {
        m_service.addUser(m_userJaneDoe);

        Message msg = new Message(JANE_DOE, "message");
        m_service.sendMessage(m_userJaneDoe, msg);

        List<Message> messages = m_service.getMessagesFor(m_userJaneDoe);
        assertNotNull(messages);
        assertTrue(messages.contains(msg));
    }

    public void testGetMessagesForExistingUserAreSortedByIdOk() {
        m_service.addUser(m_userJaneDoe);

        Message msg1 = new Message(JANE_DOE, "message1");
        Message msg2 = new Message(JANE_DOE, "message2");
        Message msg3 = new Message(JANE_DOE, "message3");

        m_service.sendMessage(m_userJaneDoe, msg2);
        m_service.sendMessage(m_userJaneDoe, msg3);
        m_service.sendMessage(m_userJaneDoe, msg1);

        List<Message> messages = m_service.getMessagesFor(m_userJaneDoe);
        assertNotNull(messages);
        assertEquals(msg1, messages.get(0));
        assertEquals(msg2, messages.get(1));
        assertEquals(msg3, messages.get(2));
    }

    public void testGetMessageByIdForNullUserFail() {
        try {
            m_service.getMessageById(null, 1L);
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testGetMessageByIdForInvalidIdFail() {
        m_service.addUser(m_userJaneDoe);

        try {
            m_service.getMessageById(m_userJaneDoe, 0L);
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testGetMessageByIdForNonExistingUserFail() {
        try {
            m_service.getMessageById(m_userJaneDoe, 1L);
            fail("Expected IllegalArgumentException!");
        }
        catch (IllegalArgumentException exception) {
            // Ok; expected...
        }
    }

    public void testGetMessageByIdForExistingUserAndExistingMessageOk() {
        m_service.addUser(m_userJaneDoe);

        Message msg = new Message(JANE_DOE, "message");
        m_service.sendMessage(m_userJaneDoe, msg);

        Message message = m_service.getMessageById(m_userJaneDoe, msg.getId());
        assertNotNull(message);
        assertEquals(msg, message);
    }

    public void testGetMessageByIdForExistingUserAndNonExistingMessageOk() {
        m_service.addUser(m_userJaneDoe);

        Message message = m_service.getMessageById(m_userJaneDoe, 1L);
        assertNull(message);
    }

    @Override
    protected void setUp() throws Exception {
        m_service = new ChatService();

        m_userJohnDoe = new User(JOHN_DOE);
        m_userJaneDoe = new User(JANE_DOE);
    }
}
