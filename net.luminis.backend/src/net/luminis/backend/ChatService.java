package net.luminis.backend;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ChatService {
    static class MessageComparator implements Comparator<Message> {
        @Override
        public int compare(Message o1, Message o2) {
            return o1.getId() < o2.getId() ? -1 : o1.getId() > o2.getId() ? 1 : 0;
        }
    }

    private final ConcurrentMap<User, ConcurrentMap<Long, Message>> m_repository;
    private final MessageComparator m_messageComparator;

    public ChatService() {
        m_repository = new ConcurrentHashMap<>();
        m_messageComparator = new MessageComparator();
    }

    public void addUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("User cannot be null!");
        }

        ConcurrentMap<Long, Message> messages = new ConcurrentHashMap<>();
        if (m_repository.putIfAbsent(user, messages) != null) {
            throw new IllegalStateException("User already joined!");
        }
    }

    public void removeUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("User cannot be null!");
        }
        if (m_repository.remove(user) == null) {
            throw new IllegalStateException("User never joined!");
        }
    }

    public List<User> getUsers() {
        return new ArrayList<>(m_repository.keySet());
    }

    public void sendMessage(User recipient, Message message) {
        assertValidUser(recipient);
        assertValidMessage(message);

        ConcurrentMap<Long, Message> messages = m_repository.get(recipient);
        if (messages == null) {
            throw new RuntimeException("Initialization of recipient was not correct!");
        }
        if (messages.put(message.getId(), message) != null) {
            throw new IllegalStateException("Duplicate message!");
        }
    }

    public List<Message> getMessagesFor(User user) {
        assertValidUser(user);

        ConcurrentMap<Long, Message> map = m_repository.get(user);
        if (map == null) {
            return Collections.emptyList();
        }

        List<Message> messages = new ArrayList<Message>(map.values());
        Collections.sort(messages, m_messageComparator);

        return messages;
    }

    public Message getMessageById(User user, long id) {
        assertValidUser(user);
        assertValidMessageId(id);

        ConcurrentMap<Long, Message> map = m_repository.get(user);
        if (map == null) {
            return null;
        }
        return map.get(id);
    }

    private void assertValidUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("User cannot be null!");
        }
        if (!m_repository.containsKey(user)) {
            throw new IllegalArgumentException("No such user!");
        }
    }

    private void assertValidMessageId(long id) {
        if (id <= 0L) {
            throw new IllegalArgumentException("Invalid message id!");
        }
    }

    private void assertValidMessage(Message msg) {
        if (msg == null) {
            throw new IllegalArgumentException("Message cannot be null!");
        }

        User sender = new User(msg.getSender());
        if ((sender == null) || !m_repository.containsKey(sender)) {
            throw new IllegalArgumentException("Invalid message sender!");
        }

        assertValidMessageId(msg.getId());

        String timestamp = msg.getTimestamp();
        if (timestamp == null) {
            throw new IllegalArgumentException("Invalid timestamp!");
        }

        String text = msg.getContent();
        if (text == null) {
            throw new IllegalArgumentException("Invalid message!");
        }
    }
}
