package net.luminis.backend;

import org.amdatu.web.rest.doc.Description;

public class PlainMessage {
    @Description("The sender of the message.")
    private String sender;
    @Description("The content of the message")
    private String content;

    public PlainMessage() {
    }

    public PlainMessage(String sender, String content) {
        this.sender = sender;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getSender() {
        return sender;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	        return true;
	    }
	    if (obj == null || getClass() != obj.getClass()) {
	        return false;
	    }
	    PlainMessage other = (PlainMessage) obj;
	    if (content == null) {
	        if (other.content != null) {
	            return false;
	        }
	    }
	    else if (!content.equals(other.content)) {
	        return false;
	    }
	    if (sender == null) {
	        if (other.sender != null) {
	            return false;
	        }
	    }
	    else if (!sender.equals(other.sender)) {
	        return false;
	    }
	    return true;
	}

	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((content == null) ? 0 : content.hashCode());
	    result = prime * result + ((sender == null) ? 0 : sender.hashCode());
	    return result;
	}

	@Override
    public String toString() {
        return "content[" + sender + ": " + content + "]";
    }
}
