package net.luminis.backend;

import org.amdatu.web.rest.doc.Description;

public class User {
	@Description("The name of the user.")
    private String name;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	        return true;
	    }
	    if (obj == null || getClass() != obj.getClass()) {
	        return false;
	    }
	
	    User other = (User) obj;
	    if (name == null) {
	        if (other.name != null) {
	            return false;
	        }
	    }
	    else if (!name.equals(other.name)) {
	        return false;
	    }
	
	    return true;
	}

	@Override
	public int hashCode() {
	    return 37 + ((name == null) ? 0 : name.hashCode());
	}

	@Override
    public String toString() {
        return "User[" + name + "]";
    }
}
