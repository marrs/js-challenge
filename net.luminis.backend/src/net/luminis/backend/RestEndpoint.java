package net.luminis.backend;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.amdatu.web.rest.doc.Description;

@Path("server")
public class RestEndpoint {
    private final ChatService m_service = new ChatService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Description("Create a new user.")
    public User createUser(@Description("The user to create.") User user) {
        try {
            m_service.addUser(user);
            return user;
        }
        catch (IllegalStateException exception) {
            throw asWebException(exception, Status.CONFLICT);
        }
        catch (IllegalArgumentException exception) {
            throw asWebException(exception);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Description("Retrieve all users.")
    public List<User> listUsers() {
        return m_service.getUsers();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{recipient}")
    @Description("Send a message to a user.")
    public Message sendMessage(
		@PathParam("recipient") @Description("The name of the user.") String recipient, 
		@Description("The message to send.") PlainMessage message) {
        try {
            Message result = new Message(message);
			m_service.sendMessage(new User(recipient), result);
			return result;
        }
        catch (IllegalArgumentException exception) {
            throw asWebException(exception);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{recipient}")
    @Description("Retrieve all messages sent to a user.")
    public List<Message> getMessages(@PathParam("recipient") @Description("The name of the user.") String user) {
        try {
            return m_service.getMessagesFor(new User(user));
        }
        catch (IllegalArgumentException exception) {
            throw asWebException(exception);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{recipient}/{id}")
    @Description("Retrieve a single message sent to a user.")
    public Message getMessage(
		@PathParam("recipient") @Description("The name of the user.") String user,
		@PathParam("id") @Description("The unique ID of the message.") long id) {
        try {
            return m_service.getMessageById(new User(user), id);
        }
        catch (IllegalArgumentException exception) {
            throw asWebException(exception);
        }
    }

    private static WebApplicationException asWebException(IllegalArgumentException exception) {
        return asWebException(exception, Status.BAD_REQUEST);
    }

    private static WebApplicationException asWebException(Exception exception, Status status) {
        return new WebApplicationException(Response.status(status).entity(exception.getMessage()).type(MediaType.TEXT_PLAIN).build());
    }
}
