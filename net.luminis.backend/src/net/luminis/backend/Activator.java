package net.luminis.backend;

import java.util.Properties;

import javax.servlet.Filter;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {
	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		// CORS filter (see http://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
		Properties props = new Properties();
		props.put("pattern", "/.*");
		manager.add(createComponent()
			.setInterface(Filter.class.getName(), props)
			.setImplementation(CrossOriginFilter.class)
			.add(createServiceDependency().setService(LogService.class).setRequired(false))
		);
		manager.add(createComponent()
			.setInterface(Object.class.getName(), null)
			.setImplementation(RestEndpoint.class)
		);
	}

	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception {
	}
}
