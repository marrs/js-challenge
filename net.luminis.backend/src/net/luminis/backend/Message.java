package net.luminis.backend;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

import org.amdatu.web.rest.doc.Description;

public class Message {
    private static final AtomicLong m_highestId = new AtomicLong(0L);

    @Description("The unique ID of the message.")
    private long id;
    @Description("The time at which the message was sent.")
    private String timestamp;
    @Description("The sender of the message.")
    private String sender;
    @Description("The content of the message")
    private String content;

    public Message() {
    }
    
    public Message(PlainMessage message) {
    	this(message.getSender(), message.getContent());
    }

    public Message(String sender, String content) {
        id = createId();

        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US);
        timestamp = formatter.format(new Date());
        this.sender = sender;
        this.content = content;
    }

    public static long createId() {
        return m_highestId.incrementAndGet();
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getSender() {
        return sender;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setContent(String message) {
        this.content = message;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	        return true;
	    }
	    if (obj == null || getClass() != obj.getClass()) {
	        return false;
	    }
	
	    Message other = (Message) obj;
	    if (id != other.id) {
	        return false;
	    }
	
	    if (content == null) {
	        if (other.content != null) {
	            return false;
	        }
	    }
	    else if (!content.equals(other.content)) {
	        return false;
	    }
	
	    if (sender == null) {
	        if (other.sender != null) {
	            return false;
	        }
	    }
	    else if (!sender.equals(other.sender)) {
	        return false;
	    }
	
	    if (timestamp == null) {
	        if (other.timestamp != null) {
	            return false;
	        }
	    }
	    else if (!timestamp.equals(other.timestamp)) {
	        return false;
	    }
	
	    return true;
	}

	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + (int) (id ^ (id >>> 32));
	    result = prime * result + ((content == null) ? 0 : content.hashCode());
	    result = prime * result + ((sender == null) ? 0 : sender.hashCode());
	    result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
	    return result;
	}

	@Override
    public String toString() {
        return "Message[" + id + " " + content + "]";
    }
}
